package com.m3.training.exceptions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileNotFoundDemo {

	public FileNotFoundDemo() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String args[]) throws IOException  {

		String fileName = "c://linesDefinitelyNothere.txt";

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			stream.forEach(System.out::println);

		} 
	}

}
