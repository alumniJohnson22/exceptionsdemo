package com.m3.training.exceptions;

import com.m3.training.loadingriding.Animal;
import com.m3.training.loadingriding.Horse;
import com.m3.training.loadingriding.Snake;

public class ClassCastDemo {

	public ClassCastDemo() {

	}

	public static void main(String[] args) {
		Animal animal = new Horse();
		// many lines of code later
		animal = new Snake();
		// guarded downcast
		if (animal instanceof Snake) {
			Snake snake = (Snake) animal;
			System.out.println(snake);
		}
		// unguarded downcast (bad practice)
		Horse horse = (Horse) animal;
		System.out.println(horse);
	}

}
