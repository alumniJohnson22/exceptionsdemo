package com.m3.training.exceptions;

public class MThreeApplicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MThreeApplicationException() {
		// TODO Auto-generated constructor stub
	}

	public MThreeApplicationException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MThreeApplicationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MThreeApplicationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MThreeApplicationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
