package com.m3.training.exceptions;

import java.io.IOException;

public class DemoExceptions {

	public DemoExceptions() {
	}

	public static void main(String[] args) throws IOException, MThreeApplicationException {
		// unchecked exception that I am checking anyway
		//new NullPointerDemo();
		// on success, continue here
		//System.out.println("more stuff happens here");

		// either way, more code continues here
		//System.out.println("beyond try catch block");

		// checked exception
		//FileNotFoundDemo.main(args);
		M3EDemo.main(args);

	}

}
